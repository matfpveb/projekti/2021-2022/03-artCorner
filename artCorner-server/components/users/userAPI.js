const express = require('express');

const router = express.Router();

const userController = require('./userController');
const authentication = require('../utils/authentication');


router.post('/register', userController.registerUser);
router.get('/', userController.getUsers);
router.post('/login', authentication.canAuthenticate, userController.loginUser);
router.delete('/:userId', userController.deleteUser);
router.patch('/', authentication.isAuthenticated, userController.changeUserInfoData);
router.patch('/profile-image', authentication.isAuthenticated, userController.changeUserProfileImage);




module.exports = router;
