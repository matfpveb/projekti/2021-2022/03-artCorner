const mongoose = require('mongoose');
const jwtUtil = require('../utils/jwt');
const { ObjectId } = require('mongodb');


const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    lastname: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    email: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    imgUrl:{
        type: mongoose.Schema.Types.String,
        required: true,
        default: '1641895768001_default_user.jpg',
    },    
    reservations:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Performance',
        required: true,
        default: []
    }],
    admin: {
        type: mongoose.Schema.Types.Boolean,
        require: true, 
        default : false
    }


});

const User = mongoose.model('User', userSchema);

async function getUserJWTByEmail(email){
    const user = await getUserByEmail(email);
    if (!user) {
      throw new Error(`User with username ${email} does not exist!`);
    }
    
    return jwtUtil.generateJWT({
        id: user._id,
        name: user.name,
        lastname: user.lastname,
        email: user.email,
        password: user.password,
        imgUrl: user.imgUrl,
        //admin: user.admin,
        
    });
}
async function getUserJWTByEmailAndPass(email, password){
    const user = await getUserByEmailAndPass(email, password);
    if (user===null) {
      throw new Error(`User with username ${email} or ${password} does not exist!`);
    }
    return jwtUtil.generateJWT({
        id: user._id,
        name: user.name,
        lastname: user.lastname,
        email: user.email,
        password: user.password,
        imgUrl: user.imgUrl,
        admin: user.admin,
        
    });
}
async function getUserJWTByUsername(username) {
    const user = await getUserByUsername(username);
    if (!user) {
      throw new Error(`User with username ${username} does not exist!`);
    }
    return jwtUtil.generateJWT({
      id: user.id,
      name: user.name,
      lastname: user.lastname,
      email: user.email,
      password: user.password,
      imgUrl: user.imgUrl
      
    });
  }
  
async function getUserJWTById(id){
    const user = await getUserById(id);
    if (!user) {
        throw new Error(`User with id ${id} does not exist!`);
      }
    
    return jwtUtil.generateJWT({
        id: user.id,
        name: user.name,
        lastname: user.lastname,
        email: user.email,
        password: user.password,
        

        
    });
}

async function registerNewUser(name, lastname, email, password, admin) {
    const user = await getUserByEmail(email);
    if (user) {
        throw new Error(`User with email ${email} already exist!`);
    }
    const u = new User();
    u._id = new mongoose.Types.ObjectId(),
    u.name = name;
    u.lastname = lastname;
    u.email = email;
    u.password = password;

    if(admin !== undefined){
        u.admin = true;
    }
   
    await u.save(); 
    return getUserJWTByEmail(email);

}
  

async function getUserByEmail(email){
    const user = await User.findOne({email: email}).exec();
    return user != undefined ? user : null;

}
async function getUserByEmailAndPass(email, password){
    const user = await User.findOne({email: email, password: password}).exec();
    return user != undefined ? user : null;

}
async function getUserById(id) {
    const user = await User.findOne({_id: id }).exec();
    return user != undefined ? user : null;
  }
  
async function getAllUsers(){
    return await User.find({}).exec();
}

async function deleteUserById(userId){
     await User.deleteOne({_id: userId}).exec();
}
//TO DO:
async function updateUserData(id,  name, lastname, email, password) {
    const user = await getUserById(id);
    //console.log(user);
    user.name = name;
    user.lastname = lastname;
    user.email = email;
    user.password = password;
    await user.save(); 
    return getUserJWTByEmail(email);
    
  }
  
  /**
   * Azurira korisnikovu profilnu sliku.
   * @param {string} userId Identifikator korisnika.
   * @param {string} imgUrl Putanja slike na serveru.
   */
async function setUserProfileImage(email, imgUrl) {
    const user = await  getUserByEmail(email);
    user.imgUrl = imgUrl;
    await user.save();
    return getUserJWTByEmail(email);

}
  
module.exports = {
    User,
    getUserByEmail,
    getUserById,
    getAllUsers,
    deleteUserById,
    registerNewUser,
    setUserProfileImage,
    getUserJWTByEmail,
    updateUserData,
    getUserJWTById,
    getUserJWTByEmailAndPass,
    getUserJWTByUsername
    
};
  
