const User = require('./userModel');
const { uploadFile } = require('../upload/uploadController');

const registerUser = async (req, res, next) => {
    const name = req.body.name;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const password = req.body.password;
    const admin = req.body.admin;


    if(req.body.admin !== undefined){
      admin = true;
    }

    try {
      if (!name || !password || !email || !lastname) {
        console.log(name + " " + lastname + " " + email + " " +  password);
        const error = new Error('Please provide all data for a new user');
        error.status = 400;
        throw error;
      }
  
      const jwt = await User.registerNewUser(name, lastname, email, password, admin);
      return res.status(201).json({
        token: jwt,
      }
  );
    } catch (err) {
      next(err);
    }
};


const loginUser = async (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  //console.log(email);
  try{
    if(!email || !password){
      const error = new Error("Niste poslali email ili lozinku");
      error.status = 400;
      throw error;
    }
    const jwt = await User.getUserJWTByEmailAndPass(email, password);
    return res.status(201).json({
      token: jwt,
    });
  } catch (err) {
    next(err);
  }
};


const getUsers = async (req, res, next) => {
  try{
    const users = await User.getAllUsers();
    res.status(200).json(users);
  }catch(err){
    next(err);
  }
  

};
const deleteUser = async (req, res, next) => {
  const id = req.params.userId;

  if (!id) {
    const error = new Error("Niste poslali id");
    error.status = 400;
    throw error;
  } else {
    const isDeleted = User.deleteUserById(id);
    if (isDeleted) {
      res.status(200).json();
    } else {
      res.status(404).json();
    }
  }
};

const changeUserInfoData = async (req, res, next) => {
  const name = req.body.name;
  const lastname = req.body.lastname;
  const email = req.body.email;
  const password = req.body.password;
  const id = req.userId;

   try {
    if (!email || !name || !lastname || !password) {
      const error = new Error('Sva polja moraju biti uneta');
      error.status = 400;
      throw error;
    }
    const user = await User.getUserById(id);
    if (user === null){
      //format id-a svakako mora biti ispravan
      const error = new Error(`Korisnik nije pronadjen!`);  
      error.status = 404;
      throw error;
    }

    const jwt = await User.updateUserData(id, name, lastname,  email, password);
    return res.status(200).json({
      token: jwt, 
    });
  } catch (err) {
    next(err);
  }
};

const changeUserProfileImage = async (req, res, next) => {
  const email = req.email;
  
  try {
    
    await uploadFile(req, res);

    if (req.file == undefined) {
      const error = new Error('Morate ucitati fajl!');
      error.status = 400;
      throw error;
    }

    const imgUrl = req.file.filename;
    console.log(imgUrl);
    const jwt = await User.setUserProfileImage(email, imgUrl);
    return res.status(200).json({
      token: jwt,
    });
  } catch (err) {
    next(err);
  }


};

module.exports = {
  loginUser,
  registerUser,
  getUsers, 
  deleteUser,
  changeUserInfoData,
  changeUserProfileImage
}
  

