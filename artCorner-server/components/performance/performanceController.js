const Performance = require('./performanceModel');
const {uploadFile} = require('../upload/uploadController');

module.exports.getPerformances = async function (req, res, next) {
    const page = req.query.page;
    const limit = req.query.limit;
  
    try {
      const performances = await Performance.paginatePerformances(page, limit);
      res.status(200).json(performances);
    } catch (err) {
      next(err);
    }
  };
  


module.exports.getPerformance = async function(req, res, next){
    const performanceId = req.params.performanceId;

    try{
        const performance = await Performance.getPerformanceById(performanceId);
        if (performance === null){
            const error = new Error(`Predstava sa identifikatorom ${performanceId} nije pronadjen!`);
            error.status = 404;
            throw error;
        }
        res.status(200).json(performance);

    }catch(err){
        next(err);
    }
};


module.exports.changeNumerOfTickets = async function(req, res, next){
    const performanceId = req.body.performanceId;
    const num = req.body.num;    

    try{
        if(!performanceId || !num){
            const error = new Error("Telo put zahteva nije ispravno popunjeno! Unesite id i vrednost num");
            error.status = 400;
            throw error; 
        }

        const performance = await Performance.getPerformanceById(performanceId);
        if (performance === null){
            const error = new Error(`Predstava sa identifikatorom ${performanceId} nije pronadjen!`);
            error.status = 404;
            throw error;
        }
        
        
        const updatedPerformance = await Performance.changeNumerOfTickets(performanceId, num);
        return res.status(200).json(updatedPerformance);

        

    }catch(err){
        next(err);
    }
}


module.exports.createPerformance = async function(req, res, next){
    const name = req.body.name;
    const genre = req.body.genre;
    const imgUrl = req.body.imgUrl;
    const duration = req.body.duration;
    const date = req.body.date;
    const theater = req.body.theater;
    const shortDescription = req.body.shortDescription;
    const longDescription = req.body.longDescription;
    const price = req.body.price;
    const numberOfTickets = req.body.numberOfTickets;
    const director = req.body.director;


    try{
        if(name === '' || genre ==='' || Number.isNaN(duration) || date === '' || theater === '' || shortDescription === '' ||
           longDescription === '' || Number.isNaN(price) || director === ''){
            const error = new Error('Validacija predstave nije uspesno prosla!');
            error.status = 400;
            throw error;
           }
    
        const performance = await Performance.createPerformance(name, genre, duration, date, theater, shortDescription, longDescription, price, numberOfTickets, director, imgUrl);
        res.status(200).json(performance);

    }catch(err){
        next(err);
    };
}




module.exports.addImage = async function(req, res, next){
    const performanceId = req.params.performanceId;
    const imgUrl = req.params.imgUrl;

    await Performance.addImageToPerformance(performanceId, imgUrl);

    res.status(200).json({message: `Slika je uspesno ucitana!: ${imgUrl}`});

};

module.exports.deletePerformanceById = async function(req, res, next){
    const performanceId = req.params.performanceId;

    try{
        const performance = await Performance.getPerformanceById(performanceId);

        if(!performance){
            const error = new Error('Predstava nije pronadjena!');
            error.status = 404;
            throw error;
        }

        await Performance.deletePerformanceById(performanceId);
        res.status(200).json({ message: `Uspesno je uklonjena predstava sa nazivom: ${performance.name}!` });

    }catch(err){
        next(err);
    }
};

            
module.exports.changePerformanceData = async function(req, res, next){
    const id = req.body.id;
    const reservation = req.body.reservation;
    const add_or_delete = req.body.add_or_delete;

    try{
        if(!id || !reservation || !add_or_delete){
            const error = new Error('Moraju biti prosledjeni svi odgovarajuci podaci!');
            error.status = 400;
            throw error;
        }
        const performance = await Performance.getPerformanceById(id);
        if(performance === null){
            const error =  new Error("Predstava nije pronadjena!");
            error.status = 404;
            throw error;
        }
        const changedPerformance = await Performance.changePerformanceData(id, reservation, add_or_delete);
        return res.status(200).json(changedPerformance);
    } catch(err){
        next(err);
    }
};