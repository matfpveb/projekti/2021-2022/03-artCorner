const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const performanceSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    id_copy: {
        type: mongoose.Schema.Types.String,
        default: ""
    },
    name: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    imgUrl: {
        type: mongoose.Schema.Types.String,
        required: true,
     },
    genre: {
        type: mongoose.Schema.Types.String,
        required: true,
        enum: ['Drama', 'Comedy', 'Tragedy']
    },
    duration: {
        type: mongoose.Schema.Types.Number,
        required: true
     },
    date: {
        type: mongoose.Schema.Types.Date,
        required: true
    },
    theater: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    shortDescription: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    longDescription: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    price: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    numberOfTickets: {
        type: mongoose.Schema.Types.Number,
        required: true
    },
    director: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    reservations: {
        type: [mongoose.Schema.Types.String],
        default: []
    }

});

performanceSchema.plugin(mongoosePaginate);

const Performance = mongoose.model('Performance', performanceSchema);

async function paginatePerformances(page = 1, limit = 10) {
    return await Performance.paginate({}, { page, limit, sort: 'date'});
}
  

async function getPerformanceById(performanceId){
    return await Performance.findOne({_id: performanceId}).exec();
}


async function createPerformance(name, genre, duration, date, theater, shortDescription, longDescription, price, numberOfTickets, director, imgUrl){
    const performance = new Performance();
    performance._id = new mongoose.Types.ObjectId();
    performance.name = name;
    performance.imgUrl = imgUrl;
    performance.genre = genre;
    performance.duration = duration;
    performance.date = new Date(date);
    performance.theater = theater;
    performance.shortDescription = shortDescription;
    performance.longDescription = longDescription;
    performance.price = price;
    performance.numberOfTickets = numberOfTickets;
    performance.director = director;

    return await performance.save();

}


async function deletePerformanceById(performanceId){
    await Performance.deleteOne({_id: performanceId}).exec();
}

async function changeNumerOfTickets(performanceId, num){
    await Performance.updateOne({_id: performanceId}, {$inc: {numberOfTickets: num} } ).exec();
    const p = await Performance.findOne({_id: performanceId}).exec();

    return p;

}


async function addImageToPerformance(performanceId, imgUrl){
    const performance = await Performance.findOne({_id: performanceId}).exec();
    performance.imgUrl = imgUrl;
    await performance.save();
}

async function changePerformanceData(id, reservation, add_or_delete){
    const performance = await getPerformanceById(id)

    let reservations = performance.reservations;
    if(add_or_delete === "add"){
        reservations.push(reservation);
    }
    else if(add_or_delete === "delete"){
        reservations = reservations.filter(el => el !== reservation);
    }
    
    performance.reservations = reservations;
    performance.id_copy = performance.id;
    await performance.save();

    return performance;
}

module.exports = {
    Performance,
    paginatePerformances,
    getPerformanceById,
    createPerformance,
    deletePerformanceById,
    addImageToPerformance,
    changeNumerOfTickets,
    changePerformanceData,
};


































