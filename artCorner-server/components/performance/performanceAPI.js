const express = require('express');

const controller = require('./performanceController');
const router = express.Router();



//autentifikacija treba da se doda

router.get('/', controller.getPerformances);
router.get('/:performanceId', controller.getPerformance);
router.post('/', controller.createPerformance);
router.put('/upload/:performanceId/:imgUrl', controller.addImage);
router.put('/', controller.changeNumerOfTickets);
router.delete('/:performanceId', controller.deletePerformanceById);
router.patch('/', controller.changePerformanceData);

module.exports = router;