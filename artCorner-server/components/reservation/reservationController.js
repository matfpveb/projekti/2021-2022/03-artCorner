const reservationServices = require('./reservationModel');

const getReservationsForUser = async (req, res, next) => {
    const userId = req.query.userId;
    try{
        if(userId == undefined){
            const error = new Error('Nedostaje identifikator kosrisnika');
            error.status = 400;
            throw error;
        }
        const reservations = await reservationServices.getReservationsForUser(userId);
        if(reservations == null){
            res.status(404).json();
        } 
        else{
            res.status(200).json(reservations);
        } 
    } 
    catch(error){
        next(error);
    }
};

const getReservationById = async (req, res, next) => {
    const reservationId = req.params.id;
    try{
        if(reservationId == undefined){
            const error = new Error('Nedostaje identifikator rezervacije');
            error.status = 400;
            throw error;
        }
        const reservation = await reservationServices.getReservationById(reservationId);
        if(reservation == null){
            res.status(404).json();
        } 
        else{
            res.status(200).json(reservation);
        } 
    } 
    catch(error){
        next(error);
    }
};

const createReservation = async (req, res, next) => {
    const userId = req.body.userId;
    const performanceId = req.body.performanceId;

    try{
        if(
            !userId ||
            !performanceId
        ){
            const error = new Error('Proverite prosledjene podatke!');
            error.status = 400;
            throw error;
        }

        const newPerformace = await reservationServices.createReservation(userId, performanceId);
        res.status(201).json(newPerformace);
        
    } 
    catch(error){
        next(error);
    }

};

const deleteReservationById = async (req, res, next) => {
    const reservationId = req.params.reservationId;
    try{
        const reservation = await reservationServices.getReservationById(reservationId);

        if(!reservation){
            const error = new Error("Rezervacija nije pronadjena!");
            error.status = 404;
            throw error;
        }

        await reservationServices.deleteReservationById(reservationId);
        res.status(200).json({ message: "Rezervacija je otkazana!"});

    } catch(error){
        next(error);
    }
}

module.exports = {
    getReservationsForUser,
    getReservationById,
    createReservation,
    deleteReservationById
};


