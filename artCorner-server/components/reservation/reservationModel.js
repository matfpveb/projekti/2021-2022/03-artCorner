const mongoose = require('mongoose');

const reservationSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    performance: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Performance'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
});

const Reservation = mongoose.model('Reservation', reservationSchema);


const getReservationsForUser = async (userId) => {
    const reservation = await Reservation.find().where('user').equals(userId).exec();
    return reservation;
}

const getReservationById = async(reservationId) => {
    const reservation = await Reservation.findById(reservationId).populate('performance').populate('user');
    return reservation;
}

const createReservation = async(userId, performanceId) => {
    const newReservation = new Reservation({
        _id: new mongoose.Types.ObjectId(),
        performance: performanceId,
        user: userId,
    });
    await newReservation.save();
    return newReservation;
}

const deleteReservationById = async(reservationId) => {
    await Reservation.deleteOne({_id: reservationId}).exec();
}

module.exports = {
    Reservation,
    getReservationsForUser,
    getReservationById,
    createReservation,
    deleteReservationById
}
