const express = require('express');
const reservationsController = require('./reservationController');

const router = express.Router();

router.get('/', reservationsController.getReservationsForUser);
router.get('/:id', reservationsController.getReservationById);

router.post('/', reservationsController.createReservation);

router.delete('/:reservationId', reservationsController.deleteReservationById);

module.exports = router
