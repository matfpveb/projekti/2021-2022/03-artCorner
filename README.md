# Project artCorner <img src = "artCorner-spa/src/assets/art.jpg" width = 70 height = 70>

![](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)
![](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![](https://img.shields.io/badge/Semantic_UI-563D7C?style=for-the-badge&logo=semanticui&logoColor=white)
![](https://img.shields.io/badge/Express.js-404D59?style=for-the-badge)
![](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)

## :memo: Opis:
Aplikacija za pretragu i rezervisanje karata za predstave grada Beograda. 

Aplikacijom bi mogli da se sluzimo kao:
 - ADMIN: U mogucnosti je da dodaje novi sadrzaj i sl.
 - KORISNIK: Omogucen mu je pregled celokupnog sadrzaja i uz to moze da
   rezervise karte i ima odredjene privilegije.
 - GOST: Omogucen mu je pregled, ali ne celokupnog sadrzaja, vec samo
   odabranog sadrzaja i gost ne moze da rezervise. Za rezervaciju je
   neophodno da ste registrovani korisnik.
___

## :hammer: Instalacija: 

- Neophodno je instalirati: [Node](https://nodejs.org/en/download/)

- Neophodno je instalirati: [MongoDB](https://www.mongodb.com/try/download/community)


___
## :wrench: Preuzimanje i pokretanje:
Nakon kloniranja repozitorijuma, pozicionirajte se u `artCorner-server` folder koristeci terminal i pokrenite sledecu komandu:
``` 
npm install 
``` 
Pokrenite istu komandu i kada se pozicionirate u `artCorner-spa` foleder.

- Server:
``` 
npm run nodemon
``` 

- Client:
``` 
ng serve
``` 

Pozicionirajte se u `artCorner-data` koristeci terminal i pokrenite sledece komande:
``` 
chmod +x importData.sh
./importData.sh 
```

Sada mozete posetiti link ne bi li otvorili sajt: 
``` 
http://localhost:4200/
``` 
___

## Shema baze podataka:
<table>
<tr>
<th>Users</th>
<th>Performances</th>
<th>Reservations</th>
</tr>
<tr>
<td>

 Polje          | Tip       | Opis                                    |
 ---------------| ----------|-----------------------------------------|
 _id            | ObjectId  |                                         |
 name           | String    |                                         |
 lastname       | String    |                                         |
 email          | String    |                                         |
 password       | String    |                                         |
 imgUrl         | String    |                                         |
 reservations   | ObjectId  | Sve rezervacije tog korisnika           |
 admin          | Boolean   | Indikator da li je ovaj korisnik admin  |

</td>
<td>

 Polje           | Tip      | Opis                                     |
 ----------------| ---------|-------------------------------------------------|
 _id             | ObjectId |                                                 |
 name            | String   |                                                 |
 imgUrl          | String   |                                                 |
 genre           | String   |                                                 |
 duration        | Number   | Trajanje predstave u minutima                   |
 date            | Date     | Datum odrzavanja                                |
 teather         | String   | Pozoriste u kom se odrzava predstava            |
 shortDescription| String   |                                                 |
 longDescription | String   |                                                 |
 price           | Number   |                                                 |
 numberOfTickets | Number   | Broj preostalih karata                          |
 director        | String   |                                                 |
 reservations    | [String] | Niz korisnika koji su rezervisali ovu predstavu |

</td>
<td>

 Polje      | Tip      | Opis                          |
 -----------|----------|-------------------------------|
 _id        | ObjectId |                               |
 performance| ObjectId | Koja predstava je rezervisana |
user        | ObjectId | Koji korisnik je rezervisao   |

</td>
</tr>
</table>

___

## Admin korisinik:
Da biste se ulogovali kao administrator unesite sledece informacije:
```
email: "admin@gmail.com"
password: "admin123"
```
___


## Developers

- [Katarina Dimitrijevic, 27/2018](https://gitlab.com/KatarinaDimitrijevic)
- [Svetlana Bicanin, 28/2018](https://gitlab.com/SvetlanaBicanin)
- [Mirjana Jocovic, 135/2018](https://gitlab.com/mirjanaa)
