import { Injectable } from '@angular/core';
import { IJWTTokenData } from './models/jwt-tokendata';


@Injectable({
  providedIn: 'root'
})
export class JwtService {
  //kad bismo imali vise tokena imali bismo enumerator,
  // pa bi setToken getToken primale instancu tog enumeratora

  private static readonly USER_JWT_TOKEN: string = 'USER_JWT_TOKEN';


  constructor() { }

  public getToken(): string | null {
    const token: string | null = localStorage.getItem(JwtService.USER_JWT_TOKEN);
    if (!token) {
      return null;
    }
    return token;
  }

  public setToken(jwt: string): void {
    localStorage.setItem(JwtService.USER_JWT_TOKEN, jwt);
  }

  public removeToken(): void {
    localStorage.removeItem(JwtService.USER_JWT_TOKEN);
  }
  //parsiranje -> kada server vrati jwt a mi hocemo da vratimo podatke
  public getDataFromToken(): IJWTTokenData | null {
    const token = this.getToken();
    if (!token) {
      return null;
    }

    //izvlacimo drugu nisku od koje dobijamo podatke
    const payloadString: string = token.split(".")[1];
    const userDataJSON = atob(payloadString);
    const payload: IJWTTokenData = JSON.parse(userDataJSON);
    return payload;
  }



}
