import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/users/models/user.model';
import { AuthService } from 'src/app/users/services/auth.service';

@Component({
  selector: 'app-nav-manu',
  templateUrl: './nav-manu.component.html',
  styleUrls: ['./nav-manu.component.css']
})
export class NavManuComponent implements OnInit {

  public user: User|null = null;
  public userSub!: Subscription;

  constructor(private auth: AuthService) { 
    this.userSub = this.auth.user.subscribe((user: User|null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();
  }


  public userLoggedIn(): boolean {
    return this.user !== null;
  }

  public loggedInUserIsAdmin(): boolean {
    //console.log(this.user?.admin)
    return this.user !== null && this.user.admin;
  }

  ngOnInit(): void {
  }

}
