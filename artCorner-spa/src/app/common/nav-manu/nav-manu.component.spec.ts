import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavManuComponent } from './nav-manu.component';

describe('NavManuComponent', () => {
  let component: NavManuComponent;
  let fixture: ComponentFixture<NavManuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavManuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavManuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
