import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { NameLastnameValidator } from './../validators/name-lastname.validator';
import { AuthService } from '../services/auth.service';

import {Subscription } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  sub!: Subscription;

  constructor(private authServices: AuthService, private router: Router) {
    this.registerForm = new FormGroup({
      name: new FormControl("", [Validators.required, NameLastnameValidator]),
      lastname: new FormControl("", [Validators.required, NameLastnameValidator]),
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)])
    })
  }

  ngOnInit(): void {
  }


  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }
  public nameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.registerForm.get('name')?.errors;
    return errors !== null;
  }
  public lastnameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.registerForm.get('lastname')?.errors;
    return errors !== null;
  }
  public emailHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.registerForm.get('email')?.errors;
    return errors !== null;
  }
  public passwordHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.registerForm.get('password')?.errors;
    return errors !== null;
  }

  public nameErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined = this.registerForm.get('name')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];

    if(errors['required']){
      errorMessages.push('Korisnik mora da unese ime!');
    }

    if(errors['nameLastname']){
      errorMessages.push(errors['nameLastname'].message);
    }
    return errorMessages;


  }
  public lastnameErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.registerForm.get('lastname')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese prezime!');
    }

    if(errors['nameLastname']){
      errorMessages.push(errors['nameLastname'].message);
    }
    return errorMessages;


  }

  public emailErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.registerForm.get('email')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese email!');
    }

    if(errors['email']){
      errorMessages.push('Neispravan format email-a!');
    }
    return errorMessages;


  }


  public passwordErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.registerForm.get('password')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese lozinku!');
    }

    if(errors['pattern']){
      errorMessages.push('Neispravan format lozinku!');
    }
    return errorMessages;


  }


  public register(): void{
    if (this.registerForm.invalid) {
      window.alert("Formular nije validno popunje!");
      return;
    }



    const data = this.registerForm.value;
    this.sub = this.authServices.postRegisterUser(data.name, data.lastname, data.email, data.password).subscribe((res:any) => {
      this.router.navigateByUrl("/");
    },
    (err:any) => {


        window.alert("Greska, vec postoji korisnik sa datim email-om");
        return;

    }
    )

  }
}
