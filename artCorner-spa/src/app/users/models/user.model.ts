import { environment } from "src/environments/environment";
export class User{
    constructor(
        public id: string,
        public name: string,
        public lastname: string,
        public email: string,
        public password: string,
        public imgUrl: string ,
        public reservations: string[] = [],
        public admin: boolean,
    ){}
    public getImageUrl(): string {
      return `${environment.fileDownloadUrl}${this.imgUrl}`;
  }



}
export interface IUser {
  id: string,
  name: string,
  lastname: string,
  email: string,
  password: string,
  imgUrl: string,
  reservations: string[],
  admin: boolean,
}
