import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const NameLastnameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    const nameLastnameParts: string[] = control.value
      .trim()
      .split(' ')
      .filter((nameLastnamePart: string) => nameLastnamePart.trim().length > 0);
    if (control.value === null) {
      return { nameLastname: { message: 'Ime/Prezime mora biti uneto!' } };
    }

    if (nameLastnameParts.length === 0) {
      return { nameLastname: { message: 'Ime/Prezime ne sme biti sastavljeno samo od praznina!' } };
    }
    if (nameLastnameParts.length > 1) {
        return { nameLastname: { message: 'Ime/Prezime ne sme biti sastavljeno od vise reci!' } };
      }
    if (nameLastnameParts.every((nameLastnamePart: string) => nameLastnamePart.match(/^[0-9\s]+$/))) {
      return { nameLastname: { message: 'Ime/Prezime ne sme biti sastavljeno samo od brojeva!' } };
    }

    return null;
  };
