import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators , ValidationErrors} from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';





@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  sub!: Subscription;

  constructor(private authServices: AuthService, private router: Router) {
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
    });
  }


  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }

  ngOnInit(): void {
  }


  public emailHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.loginForm.get('email')?.errors;
    return errors !== null;
  }

  public emailErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.loginForm.get('email')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese email!');
    }

    if(errors['pattern']){
      errorMessages.push('Neispravan format email-a!');
    }
    return errorMessages;


  }
  public passwordHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.loginForm.get('password')?.errors;
    return errors !== null;
  }

  public passwordErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.loginForm.get('password')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese lozinku!');
    }

    if(errors['pattern']){
      errorMessages.push('Neispravan format lozinku!');
    }
    return errorMessages;


  }

  public login(): void {

    if(this.loginForm.invalid) {
      window.alert("Neispravan unos!");
      return;
    }

    const data = this.loginForm.value;

    this.sub = this.authServices.postLoginUser(data.email, data.password).subscribe((res:any) => {
      this.router.navigateByUrl("/");
    },
    (err:any) => {
      if(err.status == 500 && err.statusText == "Internal Server Error"){

        window.alert("Greska, ne postoji korisnik sa datim email-om ili lozinkom!");
        return;
      }
    }
    )
  }
}

