import { AuthService } from './../services/auth.service';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { IUser, User } from "../models/user.model";
import { Observable, Subscription } from "rxjs";
import { NameLastnameValidator } from './../validators/name-lastname.validator';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';




interface IUserFormValue {
  name: string;
  lastname: string;
  email: string;
  password: string;

}

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})


export class UserProfileComponent implements OnInit, OnDestroy {
  userSub: Subscription;
  subProfile!: Subscription;
  subImg!: Subscription;

  public showReservations: boolean;

  public user: User | null = null;
  public showChangeFields: boolean;
  public userForm!: FormGroup;
  private imageToUpload: File | null = null;

  private activeSubscriptions: Subscription[] = [];


  constructor(private userService: UserService, private auth: AuthService, private router: Router) {
    this.showChangeFields = false;
    this.showReservations = false;

    this.userSub = this.auth.user.subscribe((user: User|null) => {
      this.user = user;
      if(user){
       this.userForm = new FormGroup({
          name: new FormControl(this.user?.name, [Validators.required, NameLastnameValidator]),
          lastname: new FormControl(this.user?.lastname, [Validators.required, NameLastnameValidator]),
          email: new FormControl(this.user?.email, [Validators.required, Validators.email]),
          password: new FormControl(this.user?.password, [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
          imgUrl: new FormControl("")
        });
      }
      else{
        this.userForm = new FormGroup({
          name: new FormControl("", [Validators.required, NameLastnameValidator]),
          lastname: new FormControl("", [Validators.required, NameLastnameValidator]),
          email: new FormControl("", [Validators.required, Validators.email]),
          password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
          imgUrl: new FormControl("")
        });
      }
    });


    this.auth.sendUserDataIfExists();

  }


  ngOnDestroy(): void {
    this.userSub ? this.userSub.unsubscribe() : null;
    this.subImg ? this.subImg.unsubscribe() : null;
    this.subProfile ? this.subProfile.unsubscribe() : null;
  }

  ngOnInit(): void {
  }


  public enalbeChangeFields(): void {
    this.showChangeFields = true;
  }

  public disalbeChangeFields(): void{
    this.showChangeFields = false;
  }

  public nameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.userForm.get('name')?.errors;
    return errors !== null;
  }
  public lastnameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.userForm.get('lastname')?.errors;
    return errors !== null;
  }
  public emailHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.userForm.get('email')?.errors;
    return errors !== null;
  }
  public passwordHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.userForm.get('password')?.errors;
    return errors !== null;
  }

  public nameErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined = this.userForm.get('name')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];

    if(errors['required']){
      errorMessages.push('Korisnik mora da unese ime!');
    }

    if(errors['nameLastname']){
      errorMessages.push(errors['nameLastname'].message);
    }
    return errorMessages;


  }
  public lastnameErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.userForm.get('lastname')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese prezime!');
    }

    if(errors['nameLastname']){
      errorMessages.push(errors['nameLastname'].message);
    }
    return errorMessages;


  }

  public emailErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.userForm.get('email')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese email!');
    }

    if(errors['email']){
      errorMessages.push('Neispravan format email-a!');
    }
    return errorMessages;


  }


  public passwordErrorMessage(): string[] {
    const errors: ValidationErrors | null | undefined  = this.userForm.get('password')?.errors;
    if(errors == null){
      return[];
    }

    const errorMessages: string[] = [];
    if(errors['required']){
      errorMessages.push('Korisnik mora da unese lozinku!');
    }

    if(errors['pattern']){
      errorMessages.push('Neispravan format lozinku!');
    }
    return errorMessages;
  }
  public enableReservations(): void{
    this.showReservations = true;
  }

  public disableReservations(): void{
    this.showReservations = false;
  }


  public handleFileInput(event: Event): void {
    const files: FileList | null= (event.target as HTMLInputElement).files;
    if (!files || files.length === 0) {
      this.imageToUpload = null;
      return;
    }
    this.imageToUpload = files[0];
  }

  public onSubmitUserForm() {
    if (this.userForm.invalid) {
      window.alert('Form is not valid!');
      return;
    }



    const data = this.userForm.value;
    this.subProfile = this.userService.patchChangeUserProfile(data.name, data.lastname, data.email, data.password).subscribe((user: User|null) => {
    if(!this.user) this.user = new User('','','','','', '',[], true);

      this.user.name = user!.name;
      this.user.lastname = user!.lastname;
      this.user.email = user!.email;
      this.user.password = user!.password;
      this.user.imgUrl = user!.imgUrl;



      if (this.imageToUpload !== null) {
          this.subImg = this.userService.patchUserProfileImage(this.imageToUpload).subscribe((user: User|null) => {
            this.user!.imgUrl = user!.imgUrl;
            this.user = user;
            this.imageToUpload = null;

        });
      }

      this.userForm.reset({
          name: "",
          lastname: "",
          email: "",
          password: ""
      });


      this.disalbeChangeFields();
      console.log(this.user);
    });
  }

  getUserImage(): string {
    if(!this.user) {
      return 'assets/default_user.jpg';
    }
    return this.user.getImageUrl();

  }


  logout(): void {

    this.auth.logout();
    this.router.navigateByUrl("/");
  }



}



