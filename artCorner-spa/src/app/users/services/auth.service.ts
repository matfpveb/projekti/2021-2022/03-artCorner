import { User} from './../models/user.model';
import { IJWTTokenData } from './../../common/services/models/jwt-tokendata';
import { JwtService } from './../../common/services/jwt.service';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map , tap} from "rxjs/operators";
import { Injectable } from '@angular/core';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly url = {
    registerUser: "http://localhost:3000/api/users/register",
    loginUser: "http://localhost:3000/api/users/login",
  };


  private readonly userSubject: Subject<User | null> = new Subject<User | null>();

  public readonly user: Observable<User | null> = this.userSubject.asObservable();
  public isLoggedIn: boolean = false;

  constructor(private http: HttpClient, private jwtService: JwtService) { }

  public sendUserDataIfExists(): User | null {
    const payload: IJWTTokenData | null = this.jwtService.getDataFromToken();
    if(!payload) {
      return null;
    }

    const newUser: User =  new User(payload.id, payload.name, payload.lastname , payload.email, payload.password, payload.imgUrl, [], payload.admin);
    this.userSubject.next(newUser);
    if(newUser === null){
      this.isLoggedIn = false;
    }
    else{
      this.isLoggedIn = true;
    }
    return newUser;
  }



  public postRegisterUser(name: string, lastname: string, email: string,  password: string): Observable<User | null> {
    const body = {
      name,
      lastname,
      password,
      email
    };
  const obs: Observable<{token: string}> = this.http.post<{token: string}>(this.url.registerUser, body);

  // prvo treba sacuvati token, a onda pretvoriti u korisnika
  return obs.pipe(
    tap((response: {token: string}) => this.jwtService.setToken(response.token)),
    map((response: {token: string}) => this.sendUserDataIfExists())
  )
}


  public postLoginUser(email: string, password: string): Observable<User | null> {
    const body = {
      email,
      password,
    };
    const obs: Observable<{token: string}> = this.http.post<{token: string}>(this.url.loginUser, body);

    // prvo treba sacuvati token, a onda ga 'pretvoriti' u korisnika
    return obs.pipe(
      tap((response: {token: string}) => this.jwtService.setToken(response.token)),
      map((response: {token: string}) => this.sendUserDataIfExists())
    )
  }

  public logout() {
    this.isLoggedIn = false;
    this.jwtService.removeToken();
    this.userSubject.next(null);
  }








}
