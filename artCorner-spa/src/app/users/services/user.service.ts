import { JwtService } from './../../common/services/jwt.service';
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { User   } from "../models/user.model";
import { map, tap } from "rxjs/operators";
import { AuthService } from "./auth.service";


@Injectable({
  providedIn: "root",
})
export class UserService {
  private readonly urls = {
    patchUser: "http://localhost:3000/api/users",
    patchUserProfileImage: "http://localhost:3000/api/users/profile-image",
  };

  constructor(private http: HttpClient, private authService: AuthService, private jwt: JwtService) {}


  /*public patchChangeUserProfile(userProfile: UserProfile) : Observable<User>{
    return this.http
      .patch<IUser>(this.urls.patchUser, userProfile)
      .pipe(map((u: IUser) => {return u}));
  }



  public patchUserProfileImage(file: File): Observable<User> {
    // Datoteke ne mozemo da saljemo "tek tako",
    // vec ih moramo serijalizovati kao deo FormData.
    const body: FormData = new FormData();
    body.append("file", file);

    return this.http.patch<IUser>(this.urls.patchUserProfileImage, body).pipe(
      map((u: IUser) =>  {return u}));
  }*/

  public patchChangeUserProfile(name: string, lastname: string, email: string, password: string): Observable<User | null> {
    const body = {name, lastname, email, password};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    // ovo treba da nam vrati azurirani token
    return this.http.patch<{token: string}>(this.urls.patchUser, body, {headers}).pipe(
        tap((response: {token: string}) => this.jwt.setToken(response.token)),
        map((response: {token: string}) => this.authService.sendUserDataIfExists()!)
    );
  }

  public patchUserProfileImage(file: File): Observable<User | null> {
    const body: FormData = new FormData();
    body.append('file', file);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    // za ovo nam je takodje potrebna autorizacija, pa ce izmena u odnosu na prethodni ovakav deo iz pathUser biti samo u putanji koja se poziva
    return this.http.patch<{token: string}>(this.urls.patchUserProfileImage, body, {headers}).pipe(
        tap((response: {token: string}) => this.jwt.setToken(response.token)),
        map((response: {token: string}) => this.authService.sendUserDataIfExists()!)
    );

  }
  private handleError(error: HttpErrorResponse): Observable<{ token: string | null }> | undefined{
    const serverError: { message: string; status: number; stack: string } = error.error;
    window.alert(`There was an error: ${serverError.message}. Server returned code: ${serverError.status}`);
    if (error !== null)
        return;
    return of({ token: this.jwt.getToken() });
  }

  private mapResponseToUser(response: { token: string }): User | null{
    this.jwt.setToken(response.token);
    return this.authService.sendUserDataIfExists();
  }


}
