import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreatePerformnaceComponent } from './performance/create-performnace/create-performnace.component';
import { PerformanceDetailsComponent } from './performance/performance-details/performance-details.component';
import { PerformanceShortDescriptionComponent } from './performance/performance-short-description/performance-short-description.component';
import { PopularPerformanceComponent } from './performance/popular-performance/popular-performance.component';
import { UserAuthenticatedGuard } from './users/guards/user-authenticated.guard';
import { LogInComponent } from './users/log-in/log-in.component';
import { RegisterComponent } from './users/register/register.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';

const routes: Routes = [
  //http://localhost:4200
  { path: "", component: HomeComponent },
  //http://localhost:4200/login
  { path: "login", component: LogInComponent },
  //http://localhost:4200/register
  { path: "register", component: RegisterComponent },
  //http://localhost:4200/logout
  { path: "logout", component: HomeComponent },

  //http://localhost:4200/predstave
  { path: "predstave", component: PerformanceShortDescriptionComponent},
   //http://localhost:4200/mojProfil
  { path: "mojProfil", component: UserProfileComponent, canActivate: [UserAuthenticatedGuard] },
  //http://localhost:4200/dodajPredstavu
  { path: "dodajPredstavu", component: CreatePerformnaceComponent},

  //http://localhost:4200/predstava/:id
  { path: "predstave/:performanceId", component: PerformanceDetailsComponent },
  
  // //http:localhost:4200/najpopularnijiDogadjaj
  { path: "najpopularnijiDogadjaj", component: PopularPerformanceComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
