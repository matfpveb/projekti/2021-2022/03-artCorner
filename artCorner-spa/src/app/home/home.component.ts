import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../users/services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  public isLoggedIn!: boolean;

  constructor(private router: Router, private authService: AuthService) {
    this.isLoggedIn = this.authService.isLoggedIn;
  }


  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }
  
  enableReg() {
     this.router.navigateByUrl("/register");
    
    
  }
  enableLog() {
      this.router.navigateByUrl("/login");
  }


  popularPerformance(){
    this.router.navigateByUrl("/najpopularnijiDogadjaj");
  }



}
