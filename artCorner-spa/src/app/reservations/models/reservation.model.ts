export class Reservation{
    constructor(
        public _id: string, 
        public performance: string,
        public user: string,
    ){  }
}

export interface IReservation{
    _id: string,
    performance: string,
    user: string   
}