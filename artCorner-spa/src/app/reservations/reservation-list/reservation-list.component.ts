import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Reservation } from '../models/reservation.model';
import { ReservationService } from '../services/reservation.service';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/users/models/user.model';
import { AuthService } from 'src/app/users/services/auth.service';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit, OnDestroy {
  public reservations!: Reservation[];
  private reservationSubscription?: Subscription; 
  public userSub!: Subscription;
  public user!: User | null;
  public showReservations: boolean;
  public userHasReservations: boolean = true;;
  public numberOfReservations!: number;

  constructor(private reservationService: ReservationService, private auth: AuthService) {
    this.showReservations = false;
    this.userSub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();
  }
  
  ngOnInit(): void {
    this.reservationSubscription = this.reservationService.getReservationsForUser(this.user!.id)
      .subscribe((reservations: Reservation[]) => {
        this.reservations = reservations;
        this.numberOfReservations = reservations.length;
        
        if(reservations.length > 0){
          this.userHasReservations = true;
        }
        else{
          this.userHasReservations = false;
        }
      });    
  }

  ngOnDestroy(): void {
    this.reservationSubscription ? this.reservationSubscription.unsubscribe() : null;
    this.userSub ? this.userSub.unsubscribe() : null;
  }

  public enableReservations(): void{
    this.showReservations = true;
  }

  public disableReservations(): void{
    this.showReservations = false;
  }

  public onDogadjaj(broj: number) :void{
    this.numberOfReservations--;

    if(this.numberOfReservations == 0){
      this.userHasReservations = false;
    }
  }
}