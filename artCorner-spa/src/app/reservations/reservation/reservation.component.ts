import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Reservation } from '../models/reservation.model';
import { Performance } from '../../performance/models/performance.model';
import { PerformanseService } from 'src/app/performance/services/performanse.service';
import { ReservationService } from '../services/reservation.service';
import { Observable, Subscription, switchMap } from 'rxjs';
import { User } from 'src/app/users/models/user.model';
import { AuthService } from 'src/app/users/services/auth.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit, OnDestroy {
  @Input()
  public reservation!: Reservation;
  public performance!: Performance;
  public user!: User | null;
  private performanceSub?: Subscription;
  private reservationSub?: Subscription;
  private userSub?: Subscription;
  public showReservation: boolean;

  @Output('dogadjaj') broj: EventEmitter<number> = new EventEmitter<number>();

  constructor(private performanceServis: PerformanseService, private reservationServices: ReservationService, private authService: AuthService) { 
    this.showReservation = true;
    
    this.userSub = this.authService.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.authService.sendUserDataIfExists();
  }

  ngOnInit(): void {
    this.performanceSub = this.performanceServis.getPerformanceById(this.reservation.performance).subscribe((performance: Performance) => {
      this.performance = performance;
    });
  }

  ngOnDestroy(): void{
    this.performanceSub ? this.performanceSub.unsubscribe() : null;
    this.reservationSub ? this.reservationSub.unsubscribe() : null;
    this.userSub ? this.userSub.unsubscribe() : null;
  }

  public deleteReservation(): void{

    this.broj.emit(1);

    this.performanceSub = this.performanceServis.patchPerfromance(this.performance.id_copy, this.user!.id, "delete")
      .subscribe((performance: Performance) => {
        this.performance.name = performance.name;
        this.performance.imgUrl = performance.imgUrl;
        this.performance.genre = performance.genre;
        this.performance.duration = performance.duration;
        this.performance.date = performance.date;
        this.performance.theater = performance.theater;
        this.performance.shortDescription = performance.shortDescription;
        this.performance.longDescription = performance.longDescription;
        this.performance.price = performance.price;
        this.performance.numberOfTickets = performance.numberOfTickets;
        this.performance.director = performance.director;
        this.performance.reservations = performance.reservations;   
      });

    this.reservationSub = this.reservationServices.deleteReservationById(this.reservation._id)
      .subscribe((res: Reservation) => console.log(res) );
    this.showReservation = false;

    this.performanceServis.updateNumberOfTickets(this.performance.id_copy, 1).subscribe(() => {});
  }
  
}
