export class CreateReservation{
    performanceId!: string | null;
    userId!: string;
}

export interface ICreateReservation{
    performanceId: string,
    userId: string
}