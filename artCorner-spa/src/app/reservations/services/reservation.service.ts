import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { IPerformance } from 'src/app/performance/models/performance.model';

import { IReservation, Reservation } from '../models/reservation.model';
import { CreateReservation, ICreateReservation } from './models/create-reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private urls = {
    getReservationsForUser: "http://localhost:3000/api/reservations/",
    postReservation: "http://localhost:3000/api/reservations/",
    deleteReservationById: "http://localhost:3000/api/reservations/"
  };

  constructor(private http: HttpClient) { }

  public getReservationsForUser(userId: string): Observable<Reservation[]>{
    const params: HttpParams = new HttpParams().append("userId", userId);
    
    const observable: Observable<Reservation[]> = this.http
      .get<Reservation[]>(this.urls.getReservationsForUser, { params });
    return observable;
  }

  public postReservation(createRes: CreateReservation): Observable<Reservation>{
    return this.http
      .post<IReservation>(this.urls.postReservation, createRes)
      .pipe(map((reser: IReservation) => this.createReservation(reser)));
    
  }
  private createReservation(res: IReservation): Reservation{
    return new Reservation(
      res._id,
      res.performance,
      res.user
    );
  }

  public deleteReservationById(reservationId: string): Observable<Reservation>{
    const params: HttpParams = new HttpParams().append("reservationId", reservationId);

    const observable: Observable<Reservation> = this.http
      .delete<Reservation>(`${this.urls.deleteReservationById}/${reservationId}`, { params });
    return observable;
  }
}
