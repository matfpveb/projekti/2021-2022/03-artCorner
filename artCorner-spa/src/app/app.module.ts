import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { HomeComponent } from './home/home.component';
import { PerformanceShortDescriptionComponent } from './performance/performance-short-description/performance-short-description.component';
import { PerformanceInfoComponent } from './performance/performance-info/performance-info.component';
import { RegisterComponent } from './users/register/register.component';
import { LogInComponent } from './users/log-in/log-in.component';
import { ReservationListComponent } from './reservations/reservation-list/reservation-list.component';
import { ReservationComponent } from './reservations/reservation/reservation.component';
import { PerformanceDetailsComponent } from './performance/performance-details/performance-details.component';
import { CreatePerformnaceComponent } from './performance/create-performnace/create-performnace.component';
import { NavManuComponent } from './common/nav-manu/nav-manu.component';
import { PopularPerformanceComponent } from './performance/popular-performance/popular-performance.component';

@NgModule({
  declarations: [
    AppComponent,
    UserProfileComponent,
    HomeComponent,
    PerformanceShortDescriptionComponent,
    PerformanceInfoComponent,
    RegisterComponent,
    LogInComponent,
    ReservationListComponent,
    ReservationComponent,
    PerformanceDetailsComponent,
    CreatePerformnaceComponent,
    NavManuComponent,
    PopularPerformanceComponent,



  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,  //za dvosmerno vezivanje, ali izbegavaj ovo
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
