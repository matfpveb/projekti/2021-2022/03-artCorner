import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerformanceShortDescriptionComponent } from './performance-short-description.component';

describe('PerformanceShortDescriptionComponent', () => {
  let component: PerformanceShortDescriptionComponent;
  let fixture: ComponentFixture<PerformanceShortDescriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerformanceShortDescriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformanceShortDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
