import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Performance } from '../models/performance.model';
import { PerformanseService } from '../services/performanse.service';

@Component({
  selector: 'app-performance-short-description',
  templateUrl: './performance-short-description.component.html',
  styleUrls: ['./performance-short-description.component.css']
})
export class PerformanceShortDescriptionComponent implements OnInit, OnDestroy {

  sub!: Subscription;
  
  performancesByDate!: Observable<Performance[]>;
  dramaList: Observable<Performance[]> ;
  comedyList: Observable<Performance[]> ;
  tragedyList: Observable<Performance[]> ;
  performances: Observable<Performance[]>;

  private performanseService: PerformanseService;

  isDate: boolean;
  date!: Date;

  showDramas: boolean;
  showComedies: boolean;
  showTragedies: boolean;

  calendarVisibility: boolean = false;
  sizePerDate: number = 0;

  @ViewChild('inputDate', { static: false })
  inputDate!: ElementRef;

  constructor(performanseService: PerformanseService) {
    this.performanseService = performanseService;
    
    this.dramaList = this.performanseService.getPerformancesByGenre(1, 10, "Drama");
    this.comedyList = this.performanseService.getPerformancesByGenre(1, 10, "Comedy");
    this.tragedyList = this.performanseService.getPerformancesByGenre(1, 10, "Tragedy");
    this.performances = this.performanseService.getPerformances();
    


    this.showDramas = false;
    this.showComedies = false;
    this.showTragedies = false;

    this.isDate = false;

  }
  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }


   condition(): boolean{
     return this.showComedies || this.showTragedies || this.showDramas;
   }

   changeDramaVisibility(){
    if(!this.showDramas){
      this.showDramas = true;
      this.showTragedies = false;
      this.showComedies = false;
    }
  }

  changeComedyVisibility(){
    if(!this.showComedies){
      this.showComedies = true;
      this.showTragedies = false;
      this.showDramas = false;
    }
  }

  changeTragedyVisibility(){
    if(!this.showTragedies){
      this.showTragedies = true;
      this.showComedies = false;
      this.showDramas = false;
    }
  }

  public changeCalendarVisibility(){
    this.calendarVisibility = !this.calendarVisibility;
  }


  
  public filterUsingDate(): void {
    
    this.date = new Date ((this.inputDate.nativeElement as HTMLInputElement).value);
    this.performancesByDate = this.performanseService.getPerformancesByDate(1, 10, this.date);

    if(this.date != null){
      this.isDate = true;
    }
    
    this.sub = this.performancesByDate.subscribe((p: Performance[]) => {
      this.sizePerDate = p.length;
      if(!this.sizePerDate){
        window.alert("Nema predstava za odabrani datum!");
        return;
      }
     });

  }



  ngOnInit(): void {
  }


};