import { HttpClient, HttpParams } from '@angular/common/http';
import { IPerformance, Performance } from '../models/performance.model';

import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { GetPerformanceResponseBody } from './models/performance-pagination';
import { CreatePerformance } from './models/create-performance';
import { Reservation } from 'src/app/reservations/models/reservation.model';

@Injectable({
  providedIn: 'root'
})
export class PerformanseService {
  private urls = {
    getPerformances: "http://localhost:3000/api/performances/",
    postPerformance: "http://localhost:3000/api/performances/",
    getPerformanceById: "http://localhost:3000/api/performances/",
    updateTickets: "http://localhost:3000/api/performances/",
    patchPerformance: "http://localhost:3000/api/performances/",
  };

  constructor(private http: HttpClient) { }

  public getPerformances(page: number = 1, limit: number = 10): Observable<Performance[]>{
    const queryParams: HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());

    const observable: Observable<Performance[]> = this.http
      .get<GetPerformanceResponseBody>(this.urls.getPerformances, { params: queryParams })
      .pipe(

        map((pagination: GetPerformanceResponseBody) => {
          return pagination.docs.map(
            (doc: GetPerformanceResponseBody) =>
              new Performance(
                doc._id,
                doc.id_copy,
                doc.name,
                doc.imgUrl,
                doc.genre,
                doc.duration,
                doc.date,
                doc.theater,
                doc.shortDescription,
                doc.longDescription,
                doc.price,
                doc.numberOfTickets,
                doc.director,
                doc.reservations,
              )
          );
        })
      );
    return observable;
  }


  public getPerformancesByGenre(page: number = 1, limit: number = 10, genre: string): Observable<Performance[]>{
    const queryParams: HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());

    const observable: Observable<Performance[]> = this.http
      .get<GetPerformanceResponseBody>(this.urls.getPerformances, { params: queryParams })
      .pipe(

        map((pagination: GetPerformanceResponseBody) => {
          return pagination.docs.map(
            (doc: GetPerformanceResponseBody) =>
              new Performance(
                doc._id,
                doc.id_copy,
                doc.name,
                doc.imgUrl,
                doc.genre,
                doc.duration,
                doc.date,
                doc.theater,
                doc.shortDescription,
                doc.longDescription,
                doc.price,
                doc.numberOfTickets,
                doc.director,
                doc.reservations,
              )
          ).filter(x => {
            if(x.genre === genre){
              return true;
            }
            else{
              return false;
            }
          });
        })
      );
    return observable;
  }




  public postPerformance(createPerf: CreatePerformance): Observable<Performance> {

    return this.http
      .post<IPerformance>(this.urls.postPerformance, createPerf)
      .pipe(map((perf: IPerformance) => this.createPerformance(perf)));

  }


  private createPerformance(p: IPerformance): Performance{
    return new Performance(
      p.id,
      p.id_copy,
      p.name,
      p.imgUrl,
      p.genre,
      p.duration,
      p.date,
      p.theater,
      p.shortDescription,
      p.longDescription,
      p.price,
      p.numberOfTickets,
      p.director,
      p.reservations,
    );
  }

  public updateNumberOfTickets(performanceId: string | null, num: number): Observable<Performance> {
    const body = {performanceId, num};
    const obs: Observable<Performance> = this.http.put<Performance>(this.urls.updateTickets, body);
    return obs;
  }


  public patchPerfromance(id: string | null, reservation:string, add_or_delete: string): Observable<Performance>{
    const body = {id, reservation, add_or_delete};
    const observable: Observable<Performance> = this.http.patch<Performance>(this.urls.patchPerformance, body);
    return observable;
  }



  public getPerformanceById(performanceId: string | null): Observable<Performance>{
    const observable: Observable<Performance> = this.http
      .get<Performance>(`${this.urls.getPerformanceById}/${performanceId}`);
    
    return observable;
  }

      
  public getPerformancesByDate(page: number = 1, limit: number = 10, date: Date): Observable<Performance[]>{
    const queryParams: HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());

    const observable: Observable<Performance[]> = this.http
      .get<GetPerformanceResponseBody>(this.urls.getPerformances, { params: queryParams })
      .pipe(

        map((pagination: GetPerformanceResponseBody) => {
          return pagination.docs.map(
            (doc: GetPerformanceResponseBody) =>
              new Performance(
                doc._id,
                doc.id_copy,
                doc.name,
                doc.imgUrl,
                doc.genre,
                doc.duration,
                doc.date,
                doc.theater,
                doc.shortDescription,
                doc.longDescription,
                doc.price,
                doc.numberOfTickets,
                doc.director,
                doc.reservations,
              )
          ).filter(x => {
            const tmp_date = new Date(x.date);
            if(tmp_date.getMonth() === date.getMonth() && tmp_date.getFullYear() === date.getFullYear() && tmp_date.getDate() === date.getDate()){
              return true;
            }
            else{
              return false;
            }
          });
        })
      );
    return observable;
  }
}
