import { TestBed } from '@angular/core/testing';

import { PerformanseService } from './performanse.service';

describe('PerformanseService', () => {
  let service: PerformanseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PerformanseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
