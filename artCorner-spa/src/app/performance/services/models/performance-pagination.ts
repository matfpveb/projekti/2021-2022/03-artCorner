import { Type } from "../../models/performance-type";

export interface GetPerformanceResponseBody {
    _id: string,
    id_copy: string,
    name: string,
    imgUrl: string,
    genre: Type,
    duration: Number,
    date: Date,
    theater: string,
    shortDescription: string,
    longDescription: string,
    price: number,
    numberOfTickets: number,
    director: string,
    reservations: [string],
}

export interface GetPerformanceResponseBody {
    docs: GetPerformanceResponseBody[];
    hasNextPage: boolean;
    hasPrevPage: boolean;
    limit: number;
    nextPage?: number;
    page: number;
    pagingCounter: number;
    prevPage?: number;
    totalDocs: number;
    totalPages: number;
  }


