import { Type } from "../../models/performance-type";

export class CreatePerformance {
    name!: string;
    imgUrl!: string;
    genre!: Type;
    duration!: Number;
    date!: Date;
    theater!: string;
    shortDescription!: string;
    longDescription!: string;
    price!: number;
    numberOfTickets!: number;
    director!: string;
    //reservations!: [string];

}

export interface ICreatePerformance {
    name: string;
    imgUrl: string;
    genre: Type;
    duration: Number;
    date: Date;
    theater: string;
    shortDescription: string;
    longDescription: string;
    price: number;
    numberOfTickets: number;
    director: string;
    //reservations: [string];
}