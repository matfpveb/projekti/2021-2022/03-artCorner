import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/users/models/user.model';
import { AuthService } from 'src/app/users/services/auth.service';
import { Type } from '../models/performance-type';
import { Performance } from '../models/performance.model';
import { PerformanseService } from '../services/performanse.service';

@Component({
  selector: 'app-performance-info',
  templateUrl: './performance-info.component.html',
  styleUrls: ['./performance-info.component.css'],
})
export class PerformanceInfoComponent implements OnInit, OnDestroy {
  public userSub!: Subscription;

  PerformanceTypeEnum = Type;
  isClicked: boolean;
  isAdmin!: boolean | undefined;  
  
  @Input('performanceData')
  public performance!: Performance;
  public user: User | null = null;
  public isReserved!: boolean;


  constructor(private auth: AuthService, private performanceService: PerformanseService) { 
    this.userSub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
      this.isAdmin = user?.admin;
    });
    this.auth.sendUserDataIfExists();
    this.isClicked = false;
    
  }


  ngOnDestroy(): void {
    this.userSub ? this.userSub.unsubscribe() : null;
  }


  onClick(){
    this.isClicked = !this.isClicked;
  }

  ngOnInit(): void {
    if(this.user !== null){
      let reservations = this.performance.reservations;
    let index = reservations.indexOf(this.user!.id);
    if(index !== -1){
      this.isReserved = true;
    }
    else{
      this.isReserved = false;
    }
    }
  }

};
