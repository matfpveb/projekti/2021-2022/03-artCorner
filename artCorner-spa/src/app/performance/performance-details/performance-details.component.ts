import { Component, OnInit, OnDestroy} from '@angular/core';
import { Performance } from '../models/performance.model';
import { ReservationService } from 'src/app/reservations/services/reservation.service';
import { CreateReservation } from 'src/app/reservations/services/models/create-reservation';
import { Observable, Subscription, switchMap } from 'rxjs';
import { Reservation } from 'src/app/reservations/models/reservation.model';
import { AuthService } from 'src/app/users/services/auth.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { User } from 'src/app/users/models/user.model';
import { PerformanseService } from '../services/performanse.service';
import { Type } from '../models/performance-type';

@Component({
  selector: 'app-performance-details',
  templateUrl: './performance-details.component.html',
  styleUrls: ['./performance-details.component.css'],
  inputs: ['performance']
})
export class PerformanceDetailsComponent implements OnInit, OnDestroy {
  public reservations!: Reservation[];
  public reservation!: Reservation;
  public reservationSub!: Subscription;
  public userSub!: Subscription;
  public performanceSub!: Subscription;
  public sub!: Subscription;

  public user!: User | null;
  public isReserved!: boolean;

  public performance: Observable<Performance>;
  public perf!: Performance;
  public performanceId!: string | null;
  public numOfTickets!: number;

  PerformanceTypeEnum = Type;

  constructor(private reservationServices: ReservationService, private authService: AuthService, private activatedRoute: ActivatedRoute, private performanceService: PerformanseService) {
    this.isReserved = false;
    
    this.userSub = this.authService.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.authService.sendUserDataIfExists();

    this.performance = this.activatedRoute.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.performanceId = params.get('performanceId');
        this.performanceService.getPerformanceById(this.performanceId).subscribe((p: Performance) => {
          if(this.user! !== undefined){
            let reservations = p.reservations
            let index = reservations.indexOf(this.user!.id);
            if(index !== -1){
              this.isReserved = true;
            }
            else{
              this.isReserved = false;
            }
          }

          this.perf = p;
          this.numOfTickets = p.numberOfTickets;
        });

        if(this.user){
          this.reservationSub = this.reservationServices.getReservationsForUser(this.user!.id)
          .subscribe((reservations: Reservation[]) => {
            for(let i=0; i<reservations.length; i++){
              if(reservations[i].performance == this.performanceId){
                this.reservation = reservations[i];
              }
            }  
          });
        }
        
        return this.performanceService.getPerformanceById(this.performanceId);
        
      })
    );  
  }
 
  ngOnInit(): void {

  }

  ngOnDestroy(): void{
    this.userSub ? this.userSub.unsubscribe() : null;
    this.reservationSub ? this.reservationSub.unsubscribe() : null;
    this.performanceSub ? this.performanceSub.unsubscribe() : null;
    this.sub ? this.sub.unsubscribe() : null;
  }


  public addReservation_p(): void{
    
  }

  public addReservation(): void{

    if(this.numOfTickets <= 0){
      window.alert("Nema preostalih karata za odabranu predstavu!");
      return;
    }

    const res: CreateReservation = new CreateReservation();

    res.performanceId = this.performanceId;
    res.userId = this.user!.id;

    this.isReserved = true;

    this.performanceSub = this.performanceService.patchPerfromance(this.performanceId, this.user!.id, "add")
      .subscribe((performance: Performance) => {

        this.perf.id = this.performanceId;
        this.perf.name = performance.name;
        this.perf.imgUrl = performance.imgUrl;
        this.perf.genre = performance.genre;
        this.perf.duration = performance.duration;
        this.perf.date = performance.date;
        this.perf.theater = performance.theater;
        this.perf.shortDescription = performance.shortDescription;
        this.perf.longDescription = performance.longDescription;
        this.perf.price = performance.price;
        this.perf.numberOfTickets = performance.numberOfTickets;
        this.perf.director = performance.director;
        this.perf.reservations = performance.reservations;   
      });

    this.reservationSub = this.reservationServices.postReservation(res)
      .subscribe((r: Reservation) => {
        this.reservation = r;
        
      });

    this.sub = this.performanceService.updateNumberOfTickets(this.performanceId, -1).subscribe(() => {});

  }

  public deleteReservation(): void{ 
    this.isReserved = false;

    this.performanceSub = this.performanceService.patchPerfromance(this.performanceId, this.user!.id, "delete")
      .subscribe((performance: Performance) => {
        this.perf.id = this.performanceId;
        this.perf.name = performance.name;
        this.perf.imgUrl = performance.imgUrl;
        this.perf.genre = performance.genre;
        this.perf.duration = performance.duration;
        this.perf.date = performance.date;
        this.perf.theater = performance.theater;
        this.perf.shortDescription = performance.shortDescription;
        this.perf.longDescription = performance.longDescription;
        this.perf.price = performance.price;
        this.perf.numberOfTickets = performance.numberOfTickets;
        this.perf.director = performance.director;
        this.perf.reservations = performance.reservations;
      });


      this.reservationSub = this.reservationServices.deleteReservationById(this.reservation._id)
        .subscribe((res: Reservation) => console.log(res) );

    this.performanceService.updateNumberOfTickets(this.performanceId, 1).subscribe(() => {});


  }
}
