import {Type} from './performance-type';

export class Performance{
    constructor(
        public id: string | null,
        public id_copy: string,
        public name: string,
        public imgUrl: string,
        public genre: Type,
        public duration: Number,
        public date: Date,
        public theater: string,
        public shortDescription: string,
        public longDescription: string,
        public price: number,
        public numberOfTickets: number,
        public director: string,
        public reservations: [string],
    ){  }

};



export interface IPerformance {
    id: string,
    id_copy: string,
    name: string,
    imgUrl: string,
    genre: Type,
    duration: Number,
    date: Date,
    theater: string,
    shortDescription: string,
    longDescription: string,
    price: number,
    numberOfTickets: number,
    director: string,
    reservations: [string],
}