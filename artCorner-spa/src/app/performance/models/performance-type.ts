export enum Type {
    Drama = "Drama",
    Comedy = "Comedy",
    Tragedy = "Tragedy"
};