import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Type } from "../models/performance-type";
import { IPerformance } from '../models/performance.model';
import { CreatePerformance } from '../services/models/create-performance';
import { PerformanseService } from '../services/performanse.service';

declare const $: any;

@Component({
  selector: 'app-create-performnace',
  templateUrl: './create-performnace.component.html',
  styleUrls: ['./create-performnace.component.css']
})
export class CreatePerformnaceComponent implements OnInit, OnDestroy {
  public sub!: Subscription;

  createPerformaceForm: FormGroup;
  GenreEnum = Type;

  constructor(private formBuilder: FormBuilder, private performanceService: PerformanseService, private router: Router) {
    this.createPerformaceForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      imgUrl: ['', [Validators.required]],
      genre: [null, [Validators.required]],
      duration: ['', [Validators.required, Validators.min(0)]],
      date: ['', [Validators.required]],
      theater: ['', [Validators.required]],
      shortDescription: ['', [Validators.required, Validators.minLength(10)]],
      longDescription: ['', [Validators.required, Validators.minLength(50)]],
      price: ['', [Validators.required, Validators.min(0)]],
      numberOfTickets: ['', [Validators.required, Validators.min(0)]],
      director: ['', [Validators.required]]
    })
   }



  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }


  ngOnInit(): void {
    $(".ui.radio.checkbox").checkbox();
  }

  public nameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("name")?.errors;
    return errors !== null;
  }

  nameErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("name")?.errors;

    if(!errors){
      return errorMessages;
    }
    if(errors['required']){
      errorMessages.push("Naziv predstave je obavezno polje!");
    }

    return errorMessages;
  }

  public shortDescriptionHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("shortDescription")?.errors;
    return errors !== null;
  }

  shortDescriptionErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("shortDescription")?.errors;

    if(!errors){
      return errorMessages;
    }
    if(errors['required']){
      errorMessages.push("Kratak opis predstave je obavezno polje!");
    }
    if(errors['minlength']){
      errorMessages.push("Kratak opis predstave mora sadrzati minimalno 10 karaktera!");
    }

    return errorMessages;
  }

  public longDescriptionHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("longDescription")?.errors;
    return errors !== null;
  }

  longDescriptionErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("longDescription")?.errors;

    if(!errors){
      return errorMessages;
    }
    if(errors['required']){
      errorMessages.push("Duzi opis predstave je obavezno polje!");
    }
    if(errors['minlength']){
      errorMessages.push("Duzi opis predstave mora sadrzati minimalno 50 karaktera!");
    }

    return errorMessages;
  }

  public durationHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("duration")?.errors;
    return errors !== null;
  }

  durationErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("duration")?.errors;

    if(!errors){
      return errorMessages;
    }
    if(errors['required']){
      errorMessages.push("Trajanje predstave je obavezno polje!");
    }
    if(errors['min']){
      errorMessages.push("Trajanje mora biti pozitivan broj!");
    }

    return errorMessages;
  }

  public numberOfTicketsHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("numberOfTickets")?.errors;
    return errors !== null;
  }

  numberOfTicketsErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("numberOfTickets")?.errors;

    if(!errors){
      return errorMessages;
    }
    if(errors['required']){
      errorMessages.push("Broj karata je obavezno polje!");
    }
    if(errors['min']){
      errorMessages.push("Broj karata mora biti pozitivan broj!");
    }

    return errorMessages;
  }

  public priceHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("price")?.errors;
    return errors !== null;
  }

  priceErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors | null | undefined = this.createPerformaceForm.get("price")?.errors;

    if(!errors){
      return errorMessages;
    }
    if(errors['required']){
      errorMessages.push("Cena karata je obavezno polje!");
    }
    if(errors['min']){
      errorMessages.push("Cena karata mora biti pozitivan broj!");
    }

    return errorMessages;
  }



  onCreatePerformanceSubmit(): void{

    if(this.createPerformaceForm.invalid){
      window.alert("Formular nije validno popunjen!");
      return;
    }

    const data = this.createPerformaceForm.value as IPerformance;
    const perf: CreatePerformance = new CreatePerformance();
    perf.name = data.name;
    perf.imgUrl = data.imgUrl;
    perf.genre = data.genre;
    perf.duration = Number(data.duration);
    perf.date = data.date;
    perf.theater = data.theater;
    perf.shortDescription = data.shortDescription;
    perf.longDescription = data.longDescription;
    perf.price = Number(data.price);
    perf.numberOfTickets = Number(data.numberOfTickets);
    perf.director = data.director;

    this.sub = this.performanceService.postPerformance(perf).subscribe();
    window.alert("Nova predstava je uspesno kreirana!");
    const router = this.router;
    setTimeout(function () {
       router.navigateByUrl("/predstave");
   }, 1000); 


  }



}
