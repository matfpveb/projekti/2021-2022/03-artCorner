import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePerformnaceComponent } from './create-performnace.component';

describe('CreatePerformnaceComponent', () => {
  let component: CreatePerformnaceComponent;
  let fixture: ComponentFixture<CreatePerformnaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePerformnaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePerformnaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
