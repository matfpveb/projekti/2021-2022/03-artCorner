import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Performance } from '../models/performance.model';
import { PerformanseService } from '../services/performanse.service';

@Component({
  selector: 'app-popular-performance',
  templateUrl: './popular-performance.component.html',
  styleUrls: ['./popular-performance.component.css']
})
export class PopularPerformanceComponent implements OnInit, OnDestroy {

  performances!: Performance[];
  mostPopular!: Performance;

  sub: Subscription;

  constructor(private performanceService: PerformanseService) { 
    this.sub = this.performanceService.getPerformances().subscribe((p: Performance[]) => {
      this.performances = p;
      this.performances = this.performances.sort((a, b) => (b.reservations.length - a.reservations.length));
      this.mostPopular = this.performances[0];
      //this.performances.forEach(x => console.log(x.name + " " + x.reservations.length));

    });

  }
  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null;
  }

  ngOnInit(): void {
    
  }

}
