import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularPerformanceComponent } from './popular-performance.component';

describe('PopularPerformanceComponent', () => {
  let component: PopularPerformanceComponent;
  let fixture: ComponentFixture<PopularPerformanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopularPerformanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
