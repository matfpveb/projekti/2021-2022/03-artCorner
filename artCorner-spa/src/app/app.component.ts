import { Component, OnInit, Input, OnDestroy, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from './users/models/user.model';
import { AuthService } from './users/services/auth.service';

declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewChecked{


  public sub: Subscription;
  public user: User | null = null;
  public isLogin: boolean = false;

  isVisible: boolean;

  title = 'artCorner-spa';

  constructor(private auth: AuthService, private router: Router){
    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });

    this.auth.sendUserDataIfExists();

    this.isVisible = false
  }

  public changeCalendarVisibility(): void {
    this.isVisible = !this.isVisible;
    if(this.isVisible){
    }
  }

  ngOnInit(): void {
    this.user = null;


  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
  toggleLogin(): void {
    this.isLogin = !this.isLogin;
  }
  ngAfterViewChecked(): void{
    $('.menu .item').tab();
  }


}
